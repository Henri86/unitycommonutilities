using UnityEngine;
using UnityEngine.UI;

public sealed class WebcamInput : MonoBehaviour
{
    [SerializeField] string _deviceName = "";
    [SerializeField] Vector2Int _resolution = new Vector2Int(1280, 720);
    [SerializeField] float _frameRate = 60;
    [SerializeField] RawImage _debugDrawImage;

    private WebCamTexture _webcam;
    private RenderTexture _buffer;

    public Texture Texture => _buffer;

    public WebCamTexture WebCamTexture => _webcam;

    public Color32[] GetPixels() => WebCamTexture.GetPixels32();

    public Texture2D Texture2D
    {
        get
        {
            var texture = new Texture2D(WebCamTexture.width, WebCamTexture.height, TextureFormat.RGBA32, false);
            texture.SetPixels32(GetPixels());
            return texture;
        }
    }

    private void Start()
    {
        _webcam = new WebCamTexture(_deviceName, _resolution.x, _resolution.y, (int)_frameRate);
        _buffer = new RenderTexture(_resolution.x, _resolution.y, 0);
        _webcam.Play();
    }

    private void OnDestroy()
    {
        if (_webcam != null)
        {
            Destroy(_webcam);
        }
        if (_buffer != null)
        {
            Destroy(_buffer);
        }
    }

    private void Update()
    {
        if (!_webcam.didUpdateThisFrame)
        {
            return;
        }

        var aspect1 = (float)_webcam.width / _webcam.height;
        var aspect2 = (float)_resolution.x / _resolution.y;
        var gap = aspect2 / aspect1;

        var vflip = _webcam.videoVerticallyMirrored;
        var scale = new Vector2(gap, vflip ? -1 : 1);
        var offset = new Vector2((1 - gap) / 2, vflip ? 1 : 0);

        Graphics.Blit(_webcam, _buffer, scale, offset);
    }

    private void LateUpdate()
    {
        if (_debugDrawImage != null)
        {
            _debugDrawImage.texture = Texture;
        }
    }
}
