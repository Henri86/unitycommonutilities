﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable] public class OnCollisionEnter : UnityEvent<Collision> { }
[Serializable] public class OnCollisionExit : UnityEvent<Collision> { }
[Serializable] public class OnCollisionStay : UnityEvent<Collision> { }

[RequireComponent(typeof(Collider))]
public class CollisionEventListener : MonoBehaviour
{
    public OnCollisionEnter CollisionEnter;
    public OnCollisionExit CollisionExit;
    public OnCollisionStay CollisionStay;

    private void OnCollisionEnter(Collision collision)
    {
        CollisionEnter.Invoke(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        CollisionExit.Invoke(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        CollisionStay.Invoke(collision);
    }
}