﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable] public class OnTriggerEnter : UnityEvent<Collider> { }
[Serializable] public class OnTriggerExit : UnityEvent<Collider> { }
[Serializable] public class OnTriggerStay : UnityEvent<Collider> { }

[RequireComponent(typeof(Collider))]
public class TriggerEventListener : MonoBehaviour
{
    public OnTriggerEnter TriggerEnter;
    public OnTriggerExit TriggerExit;
    public OnTriggerStay TriggerStay;

    private void OnTriggerEnter(Collider collision)
    {
        TriggerEnter.Invoke(collision);
    }

    private void OnTriggerExit(Collider collision)
    {
        TriggerExit.Invoke(collision);
    }

    private void OnTriggerStay(Collider collision)
    {
        TriggerStay.Invoke(collision);
    }
}