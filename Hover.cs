using UnityEngine;

[DefaultExecutionOrder(2)]
public class Hover : MonoBehaviour
{
    [Header("Position")]
    [SerializeField] private bool enablePosition = true;
	[SerializeField] private Vector3 positionConstantOffset;
	[SerializeField] private bool enablePositionWobble = true;
	[SerializeField] private Vector3 positionWobble = new(0.1f, 0.1f, 0.1f);
	[SerializeField] private Vector3 positionWobbleSpeed = new(0.5f, 0.5f, 0.5f);
	[SerializeField] private bool offsetPositionValues;
	[SerializeField] private float positionInfluence = 1.0f;

	[Header("Rotation")]
    [SerializeField] private bool enableRotation = true;
    [SerializeField] private Vector3 rotationConstantOffset;
	[SerializeField] private bool enableRotationWobble = true;
	[SerializeField] private Vector3 rotationWobble = new(4.0f, 4.0f, 4.0f);
	[SerializeField] private Vector3 rotationWobbleSpeed = new(0.5f, 0.5f, 0.5f);
	[SerializeField] private bool offsetRotationValues;
	[SerializeField] private float rotationInfluence = 1.0f;

	[Header("Scale")]
    [SerializeField] private bool enableScale = true;
    [SerializeField] private Vector3 scaleConstantOffset;
	[SerializeField] private bool enableScaleWobble = true;
	[SerializeField] private Vector3 scaleWobble = new(0.1f, 0.1f, 0.1f);
	[SerializeField] private Vector3 scaleWobbleSpeed = new(0.5f, 0.5f, 0.5f);
	[SerializeField] private bool offsetScaleValues;
	[SerializeField] private float scaleInfluence = 1.0f;

	[SerializeField] private bool rigidbodyMode;

	private new Rigidbody rigidbody;
	private Vector3 basePosition;
	private Quaternion baseRotation;
	private Vector3 baseScale;
	private Vector3 phase = new();

	void Awake()
	{
		basePosition = transform.localPosition;
		baseRotation = transform.localRotation;
		baseScale = transform.localScale;

		if (rigidbodyMode)
		{
			rigidbody = GetComponent<Rigidbody>();
		}

		phase.x = Random.value * 100.0f;
		phase.y = Random.value * 100.0f;
		phase.z = Random.value * 100.0f;
	}

	private void LateUpdate()
    {
        if (enablePosition)
        {
            UpdatePosition();
        }
        if (enableRotation)
        {
            UpdateRotation();
        }
        if (enableScale)
        {
            UpdateScale();
        }
    }

    private void UpdatePosition()
    {
        Vector3 positionOffset = enablePositionWobble ? Vector3.Scale(positionWobble, (Noise.Perlin1D(phase + Time.time * positionWobbleSpeed))) : Vector3.zero;
        Vector3 updatedPosition;
        if (offsetPositionValues)
        {
            updatedPosition = transform.localPosition + positionInfluence * (positionOffset + positionConstantOffset);
        }
        else
        {
            updatedPosition = basePosition + positionInfluence * (positionOffset + positionConstantOffset);
        }

        if (rigidbodyMode)
        {
            rigidbody.MovePosition(updatedPosition - transform.localPosition);
        }
        else
        {
            transform.localPosition = updatedPosition;
        }
    }

    private void UpdateRotation()
    {
        Vector3 rotationOffset = enableRotationWobble ? Vector3.Scale(rotationWobble, (Noise.Perlin1D(phase + new Vector3(phase.y, phase.z, phase.x) + Time.time * rotationWobbleSpeed))) : Vector3.zero;
        Quaternion updatedRotation;
        if (offsetRotationValues)
        {
            if (positionInfluence != 1.0f)
            {
                updatedRotation = transform.localRotation * Quaternion.Slerp(Quaternion.identity, Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset), rotationInfluence);
            }
            else
            {
                updatedRotation = transform.localRotation * Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset);
            }
        }
        else
        {
            if (rotationInfluence != 1.0f)
            {
                updatedRotation = Quaternion.Slerp(baseRotation, baseRotation * Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset), rotationInfluence);
            }
            else
            {
                updatedRotation = baseRotation * Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset);
            }
        }

        if (rigidbodyMode)
        {
            rigidbody.MoveRotation(updatedRotation);
        }
        else
        {
            transform.localRotation = updatedRotation;
        }
    }

    private void UpdateScale()
    {
        Vector3 scaleOffset = enableScaleWobble ? Vector3.Scale(scaleWobble, (Noise.Perlin1D(phase + Time.time * scaleWobbleSpeed))) : Vector3.zero;
        Vector3 updatedScale;
        if (offsetScaleValues)
        {
            updatedScale = transform.localScale + scaleInfluence * (scaleOffset + scaleConstantOffset);
        }
        else
        {
            updatedScale = baseScale + scaleInfluence * (scaleOffset + scaleConstantOffset);
        }

        transform.localScale = updatedScale;
    }

    private Vector3 EvaluatePositionOffset()
	{
		Vector3 positionOffset = enablePositionWobble ? Vector3.Scale(positionWobble, (Noise.Perlin1D(phase + Time.time * positionWobbleSpeed))) : Vector3.zero;
		return positionInfluence * (positionOffset + positionConstantOffset);
	}

	private Quaternion EvaluateRotationOffset()
	{
		Vector3 rotationOffset = enableRotationWobble ? Vector3.Scale(rotationWobble, (Noise.Perlin1D(phase + new Vector3(phase.y, phase.z, phase.x) + Time.time * rotationWobbleSpeed))) : Vector3.zero;
		if (positionInfluence != 1.0f)
		{
			return Quaternion.Slerp(Quaternion.identity, Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset), rotationInfluence);
		}
		else
		{
			return Quaternion.Euler(rotationOffset) * Quaternion.Euler(rotationConstantOffset);
		}
	}
}