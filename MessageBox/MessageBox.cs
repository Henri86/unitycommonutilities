using System;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    [SerializeField] private GameObject buttonPrefab;
    [SerializeField] private Transform buttonParent;
    [SerializeField] private TMPro.TMP_Text header;
    [SerializeField] private TMPro.TMP_Text message;

    public void ShowMessageBox(string header, string message, params ButtonData[] buttons)
    {
        this.header.text = header;
        this.message.text = message;

        if (buttonPrefab == null)
        {
            var prefab = (GameObject)Resources.Load("MessageBox/MessageBoxButton", typeof(GameObject));
            buttonPrefab = prefab;
        }

        foreach (Transform child in buttonParent)
        {
            Destroy(child.gameObject);
        }

        foreach (var buttonData in buttons)
        {
            var buttonGameObject = Instantiate(buttonPrefab, buttonParent);
            var button = buttonGameObject.GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                buttonData.OnButtonPressed?.Invoke();
                Destroy(gameObject);
            });
            var buttonText = buttonGameObject.GetComponentInChildren<TMPro.TMP_Text>();
            buttonText.text = buttonData.Text;
        }
    }

    public static void Show(string header, string message, params ButtonData[] buttons)
    {
        var messageBoxPrefab = (GameObject)Resources.Load("MessageBox/MessageBox", typeof(GameObject));
        var messageBoxGameObject = Instantiate(messageBoxPrefab);
        messageBoxGameObject.GetComponent<MessageBox>().ShowMessageBox(header, message, buttons);
    }

    public class ButtonData
    {
        public string Text;
        public Action OnButtonPressed;

        public ButtonData(string text, Action onButtonPressed = null)
        {
            Text = text;
            OnButtonPressed = onButtonPressed;
        }
    }
}
