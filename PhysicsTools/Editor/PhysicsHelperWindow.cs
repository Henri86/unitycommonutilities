﻿using System;
using UnityEditor;
using UnityEngine;

public class PhysicsHelperWindow : EditorWindow
{
    private Vector3 forceToAdd;
    private Vector3 torqueToAdd;
    bool includeChildren = true;

    [MenuItem("Tools/Physics Helper")]
    public static void ShowWindow()
    {
        GetWindow(typeof(PhysicsHelperWindow));
    }

    private void OnGUI()
    {
        DrawSelectionSettings();

        GUILayout.BeginHorizontal();
        DrawForceAdders();
        GUILayout.EndHorizontal();
    }

    private void DrawSelectionSettings()
    {
        GUILayout.Label("Selection Settings", EditorStyles.boldLabel);
        includeChildren = EditorGUILayout.Toggle("Include children", includeChildren);
    }

    private void DrawForceAdders()
    {
        GUILayout.BeginVertical();

        GUILayout.Label("Add Forces", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        DrawForceAppliers();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    private void DrawForceAppliers()
    {
        GUILayout.BeginVertical();
        DrawForceApplier("Force", ref forceToAdd, includeChildren, (rigidbody, force) =>
        {
            rigidbody.AddForce(force, ForceMode.Force);
        });
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        DrawForceApplier("Torque", ref torqueToAdd, includeChildren, (rigidbody, force) =>
        {
            //var centerPosition = selectedGameObjects.GetCenter();
            //rigidbody.AddRelativeTorque(force, ForceMode.Force);
            rigidbody.AddTorque(force, ForceMode.Force);
        });
        GUILayout.EndVertical();
    }

    private void DrawForceApplier(string forceLabel, ref Vector3 force, bool includeChildrenz, Action<Rigidbody, Vector3> action)
    {
        force = EditorGUILayout.Vector3Field(forceLabel, force);

        if (GUILayout.Button("Apply"))
        {
            Vector3 vector3 = force;
            GameObjectHelper.ForEachSelected<Rigidbody>((rigidbody) =>
            {
                action.Invoke(rigidbody, vector3);
            }, includeChildrenz);
        }
    }
}