﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    public static class Utilities
    {
        public static float GetAnimationLength(Animator animator, string animationName)
        {
            RuntimeAnimatorController ac = animator.runtimeAnimatorController;
            foreach (AnimationClip clip in ac.animationClips)
            {
                if (clip.name.Equals(animationName))
                    return clip.length;
            }
            return 0;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this Queue<T> queue)
        {
            List<T> shuffledList = queue.ToList();
            shuffledList.Shuffle();
            queue.Clear();
            foreach (T list in shuffledList)
                queue.Enqueue(list);
        }

        public static GameObject GetChildGameObject(GameObject fromGameObject, string withName)
        {
            Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>();
            foreach (Transform t in ts) if (t.gameObject.name == withName) return t.gameObject;
            return null;
        }

        public static Color WithAlpha(this Color color, float alpha)
        {
            return new Color(color.r, color.g, color.b, alpha);
        }

        public static Vector2 DeadZone(Vector2 stickInput)
        {
            float deadzone = 0.25f;
//            Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            if (stickInput.magnitude < deadzone)
                stickInput = Vector2.zero;
            else
                stickInput = stickInput.normalized * ((stickInput.magnitude - deadzone) / (1 - deadzone));
            return stickInput;
        }

        //        //Defined in the common base class for all mono behaviours
        //        public I GetInterfaceComponent<I>() where I : class
        //        {
        //            return GetComponent(typeof(I)) as I;
        //        }

        //        public static List<I> FindObjectsOfInterface<I>() where I : class
        //        {
        //            MonoBehaviour[] monoBehaviours = FindObjectsOfType<MonoBehaviour>();
        //            List<I> list = new List<I>();
        //
        //            foreach (MonoBehaviour behaviour in monoBehaviours)
        //            {
        //                I component = behaviour.GetComponent(typeof(I)) as I;
        //
        //                if (component != null)
        //                {
        //                    list.Add(component);
        //                }
        //            }
        //
        //            return list;
        //        }

        public static T[,] TransposeRowsAndColumns<T>(this T[,] arr)
        {
            int rowCount = arr.GetLength(0);
            int columnCount = arr.GetLength(1);
            T[,] transposed = new T[columnCount, rowCount];
            if (rowCount == columnCount)
            {
                transposed = (T[,])arr.Clone();
                for (int i = 1; i < rowCount; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        T temp = transposed[i, j];
                        transposed[i, j] = transposed[j, i];
                        transposed[j, i] = temp;
                    }
                }
            }
            else
            {
                for (int column = 0; column < columnCount; column++)
                {
                    for (int row = 0; row < rowCount; row++)
                    {
                        transposed[column, row] = arr[row, column];
                    }
                }
            }
            return transposed;
        }

        public static void ForEachXy(int mapWidth, int mapHeight, Action<int, int> callback)
        {
            for (int y = 0; y < mapHeight; y++)
                for (int x = 0; x < mapWidth; x++)
                    callback(x, y);
        }

        private static void PrintBounds(Collider2D aCollider2D)
        {
            string toPrint = aCollider2D.gameObject.name;
            toPrint += "\n";
            toPrint += "Center: " + aCollider2D.bounds.center.ToString("F0");
            toPrint += "\n";
            toPrint += "Size: " + aCollider2D.bounds.size.ToString("F0");
            toPrint += "\n";
            toPrint += "Extents: " + aCollider2D.bounds.extents.ToString("F0");
            Debug.Log(toPrint);
        }

        private static void DrawBounds(Collider2D aCollider2D, Color aColor)
        {
            if (aCollider2D != null)
            {
                Gizmos.color = aColor;
                Gizmos.DrawCube(aCollider2D.bounds.center, aCollider2D.bounds.size);
            }
        }
    }
}
