﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.UnityCommonUtilities
{
    public static class RandomUtilities
    {
        public static bool RandomBool()
        {
            return UnityEngine.Random.value > 0.5f;
        }

        public static T RandomElement<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.RandomElementUsing<T>(new Random());
        }

        public static T RandomElementUsing<T>(this IEnumerable<T> enumerable, Random rand)
        {
            var index = rand.Next(0, enumerable.Count());
            return enumerable.ElementAt(index);
        }

        public static bool RandomPercentage(float aPercentage)
        {
            return !(UnityEngine.Random.value > (aPercentage / 100.0f));
        }

        public static UnityEngine.Vector2 RandomVector2(UnityEngine.Vector2 min, UnityEngine.Vector2 max)
        {
            return new UnityEngine.Vector2(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y));
        }

        public static UnityEngine.Vector3 RandomVector3(UnityEngine.Vector3 min, UnityEngine.Vector3 max)
        {
            return new UnityEngine.Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
        }
    }
}