using UnityEngine;

namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class Vector2PlayerPrefsStrategyImpl:PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            Vector2 v2=(Vector2)obj;
            float[] floatArray=new float[]{v2.x,v2.y};
            string str=ConverterUtils.convertFloatArrayToString(floatArray);
            UnityEngine.PlayerPrefs.SetString(key,str);
        }

        public object readFromPlayerPrefs (string key)
        {
            string str=UnityEngine.PlayerPrefs.GetString(key);
            float[] floatArray=ConverterUtils.convertStringToFloatArray(str);
            Vector2 v2=Vector2.zero;
            if (floatArray.Length==2){
                v2=new Vector2(floatArray[0],floatArray[1]);
			
            }
            return v2;
        }
        #endregion
    }
}
