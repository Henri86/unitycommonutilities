namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class BoolPlayerPrefsStrategyImpl : PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            bool boolObj=(bool)obj;
            UnityEngine.PlayerPrefs.SetInt(key,boolObj?1:0);
        }

        public object readFromPlayerPrefs (string key)
        {
            int result=UnityEngine.PlayerPrefs.GetInt(key);
            return result==1;		
        }
        #endregion
    }
}
