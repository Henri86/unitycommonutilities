namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public interface PlayerPrefsStrategyInterface
    {
        void writeToPlayerPrefs(string key, object obj);
        object readFromPlayerPrefs(string key);
    }
}
