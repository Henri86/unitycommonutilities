namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class IntPlayerPrefsStrategyImpl : PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            UnityEngine.PlayerPrefs.SetInt(key, (int)obj);
        }

        public object readFromPlayerPrefs (string key)
        {
            return UnityEngine.PlayerPrefs.GetInt(key);
        }
        #endregion
    }
}
