namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class FloatPlayerPrefsStrategyImpl : PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            UnityEngine.PlayerPrefs.SetFloat(key,(float)obj);
        }

        public object readFromPlayerPrefs (string key)
        {
            return UnityEngine.PlayerPrefs.GetFloat(key);
        }
        #endregion
    }
}
