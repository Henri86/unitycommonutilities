using UnityEngine;

namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class Vector3PlayerPrefsStrategyImpl:PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            Vector3 v3=(Vector3)obj;
            float[] floatArray=new float[]{v3.x,v3.y,v3.z};
            string str=ConverterUtils.convertFloatArrayToString(floatArray);
            UnityEngine.PlayerPrefs.SetString(key,str);
        }

        public object readFromPlayerPrefs (string key)
        {
            string str=UnityEngine.PlayerPrefs.GetString(key);
            float[] floatArray=ConverterUtils.convertStringToFloatArray(str);
            Vector3 v3=Vector3.zero;
            if (floatArray.Length==3){
                v3=new Vector3(floatArray[0],floatArray[1],floatArray[2]);			
            }
            return v3;
		
        }
        #endregion
    }
}
