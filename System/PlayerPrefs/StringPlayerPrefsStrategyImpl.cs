namespace Assets.Scripts.Utilities.System.PlayerPrefs
{
    public class StringPlayerPrefsStrategyImpl:PlayerPrefsStrategyInterface
    {
        #region PlayerPrefsStrategyInterface implementation
        public void writeToPlayerPrefs (string key, object obj)
        {
            UnityEngine.PlayerPrefs.SetString(key,(string)obj);
        }

        public object readFromPlayerPrefs (string key)
        {
            return UnityEngine.PlayerPrefs.GetString(key);
        }
        #endregion
    }
}
