﻿using UnityEngine;

namespace Assets.UnityCommonUtilities.System
{
    public class ContainerAssigner : MonoBehaviour
    {
        [HideInInspector]
        public string myAsssignedContainerTag;

        public void Start ()
        {
            Transform parentTransform = GameObject.FindGameObjectWithTag(myAsssignedContainerTag).transform;

            if (parentTransform != null)
            {
                transform.SetParent(parentTransform);
            }
            else
            {
                Debug.LogWarning("Container tag '" + myAsssignedContainerTag + "' was not found");
            }
        }
    }
}
