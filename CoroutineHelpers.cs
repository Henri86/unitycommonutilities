﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    public class CoroutineHelpers
    {
        public static IEnumerator WaitAction(float time, Action toRun)
        {
            yield return new WaitForSeconds(time);
            toRun?.Invoke();
        }
    }
}
