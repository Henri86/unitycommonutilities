﻿using System.Collections.Generic;
using Assets.UnityCommonUtilities;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Selector))]
public class HighlightMeshSelector : MonoBehaviour
{
    [SerializeField] private Material selectedMaterial;

    private readonly Dictionary<int, SelectedMesh> highlighted = new Dictionary<int, SelectedMesh>();

    private void Awake()
    {
        GetComponent<Selector>().OnSelectAction += gameObjectSelected => SetGameObjectSelected(true, gameObjectSelected);
        GetComponent<Selector>().OnDeselectAction += gameObjectSelected => SetGameObjectSelected(false, gameObjectSelected);
    }

    private void SetGameObjectSelected(bool selected, GameObject gameObjectToSet)
    {
        if (gameObjectToSet == null) return;

        if (selected)
        {
            var newModel = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newModel.RemoveComponent<BoxCollider>();
            var selectedMesh = newModel.AddComponent<SelectedMesh>();
            selectedMesh.SelectedObject = gameObjectToSet.transform;
            newModel.name = gameObjectToSet.name + " selection";
            newModel.transform.position = gameObjectToSet.transform.position;
            newModel.transform.rotation = gameObjectToSet.transform.rotation;
            newModel.transform.localScale = gameObjectToSet.transform.localScale;
            newModel.transform.SetParent(transform, false);
            newModel.GetComponent<MeshFilter>().mesh = gameObjectToSet.GetComponent<MeshFilter>().mesh;
            var meshRenderer = newModel.GetComponent<MeshRenderer>();
            meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
            meshRenderer.material = selectedMaterial;
            highlighted[gameObjectToSet.GetInstanceID()] = selectedMesh;
        }
        else
        {
            Destroy(highlighted[gameObjectToSet.GetInstanceID()].gameObject);
            highlighted.Remove(gameObjectToSet.GetInstanceID());
        }
    }

    internal class SelectedMesh : MonoBehaviour
    {
        public Transform SelectedObject;

        private void Update()
        {
            transform.position = SelectedObject.position;
            transform.rotation = SelectedObject.rotation;
            transform.localScale = SelectedObject.localScale;
        }
    }
}