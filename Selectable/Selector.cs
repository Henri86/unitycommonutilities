﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.UnityCommonUtilities;
using UnityEngine;
using UnityEngine.EventSystems;

public class Selector : UnitySingleton<Selector>
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private List<string> selections;

    private readonly Dictionary<int, GameObject> selectedItems = new Dictionary<int, GameObject>();

    public Action<GameObject> OnSelectAction;
    public Action<GameObject> OnDeselectAction;

    public Func<bool> SingleSelect;
    public Func<bool> MultiSelect;

    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            HandleRayCast();
    }

    public IEnumerable<GameObject> Selections => selectedItems.Values;

    public IEnumerable<T> GetSelections<T>() where T : Component
    {
        var toReturn = selectedItems.Values.Select(o => o.GetComponentInChildren<T>());
        return toReturn;
    }

    private void HandleRayCast()
    {
        if ((SingleSelect != null && SingleSelect()) || (MultiSelect != null && MultiSelect()))
        {
            if (SingleSelect != null && SingleSelect())
            {
                DeselectAll();
            }

            Raycast(Camera.main, layerMask, o =>
            {
                if (selectedItems.ContainsKey(o.GetInstanceID())) return;

                selectedItems.Add(o.GetInstanceID(), o);
                OnSelectAction?.Invoke(o);
            });
        }
    }

    private void Raycast(Camera cameraToUse, LayerMask layerMaskToUse, Action<GameObject> action)
    {
        var ray = cameraToUse.ScreenPointToRay(Input.mousePosition);
        var hits = Physics.RaycastAll(ray, 100, layerMaskToUse).ToList();
        if (!hits.Any()) return;

        hits.Sort((hit, raycastHit) => hit.distance.CompareTo(raycastHit.distance));
        action?.Invoke(hits.First().transform.gameObject);
        selections = selectedItems.Select(pair => pair.Value.name).ToList();
    }

    public void DeselectAll()
    {
        selectedItems.Values.ForEach(OnDeselectAction);
        selectedItems.Clear();
    }
}
