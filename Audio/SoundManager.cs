﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Assets.UnityCommonUtilities.Audio
{
    public class SoundManager : UnitySingleton<SoundManager>
    {
        public AudioMixer myAudioMixer;
        public AudioMixerGroup audioMixerGroup;
        private List<AudioSource> mySfxSource;
        private int maxNrSources;

        protected SoundManager()
        { }

        private void Awake()
//        private void Start()
        {
            if (mySfxSource == null)
                mySfxSource = new List<AudioSource>();
            maxNrSources = 4;
        }

        public void Play(AudioClip aAudioClip, float aPitch = 1.0f, float aVolume = 1.0f)
        {
            AudioSource source = GetFreeSource();
            if (aAudioClip == null || source == null)
            {
                Debug.LogError("SoundManager: Audio clip == null");
            }
            else
            {
                source.clip = aAudioClip;
                source.pitch = aPitch;
                source.PlayOneShot(aAudioClip, aVolume);
//                Debug.Log("SoundManager sound played: " + aAudioClip.name);
            }
        }

        private AudioSource GetFreeSource()
        {
            if (mySfxSource != null)
            {
                foreach (AudioSource audioSource in mySfxSource)
                {
                    if (!audioSource.isPlaying)
                    {
                        return audioSource;
                    }
                }
            }
            return CreateNewSource();
        }

        private AudioSource CreateNewSource()
        {
            if (mySfxSource == null) {
                return null;
            }

            if (mySfxSource.Count > maxNrSources)
            {
                Debug.LogWarning("Max number of sources reached!");
                return mySfxSource[0];
            }
            AudioSource source = gameObject.AddComponent<AudioSource>();
            source.outputAudioMixerGroup = audioMixerGroup;
            source.playOnAwake = false;
            mySfxSource.Add(source);
            return source;
        }

        public AudioMixer GetMasterMixer()
        {
			return myAudioMixer;
		}
    }
}
