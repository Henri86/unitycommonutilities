﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.UnityCommonUtilities.Audio
{
    [RequireComponent(typeof(Button))]
    public class ClickSoundSoundManagerSource : MonoBehaviour
    {
        public AudioClip myAudioClip;

        private Button myButton { get { return GetComponent<Button>(); } }

        public void Start ()
        {
            myButton.onClick.AddListener(() => PlaySound());
        }

        private void PlaySound()
        {
            SoundManager.Instance.Play(myAudioClip);
        }
    }
}
