﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.UnityCommonUtilities.Audio
{
    [RequireComponent(typeof(Button))]
    public class ClickSoundOwnSource : MonoBehaviour
    {
        public AudioClip myAudioClip;

        private Button myButton { get { return GetComponent<Button>(); } }
        private AudioSource myAudioSource { get { return GetComponent<AudioSource>(); } }

        public void Start ()
        {
            gameObject.AddComponent<AudioSource>();
            myAudioSource.clip = myAudioClip;
            myAudioSource.playOnAwake = false;

            myButton.onClick.AddListener(() => PlaySound());
        }

        private void PlaySound()
        {
            myAudioSource.PlayOneShot(myAudioClip);
        }
    }
}
