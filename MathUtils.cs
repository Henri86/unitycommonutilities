using System;
using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    public class MathUtils
    {
        public static double Clamp01(double value)
        {
            return Clamp(value, 0, 1);
        }

        public static double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        public static T Clamp<T>(T value, T min, T max) where T : IComparable
        {
            return (value.CompareTo(min) > 0) ? min : (value.CompareTo(max) > 0) ? max : value;
        }

        //        public static float DEGTORAD = 0.0174532925199432957f;
        //        public static float RADTODEG = 57.295779513082320876f;

        public static T[,] RotateLeft<T>(T[,] piece )
        {
            var res = new T[piece.GetLength(1),piece.GetLength(0)];
            for (var x = 0; x < piece.GetLength(0); x++)
            {
                for (var y = 0; y < piece.GetLength(1); y++)
                {
                    res[res.GetLength(0)-1-y,x] = piece[x,y];
                }
            }
            return res;

        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public static float SnapTo(float a, float snap)
        {
            return Mathf.Round(a / snap) * snap;
        }

        public static float MapValue(float a0, float a1, float b0, float b1, float value) => b0 + (b1 - b0) * ((value - a0) / (a1 - a0));

        //        public static int[,] RotateRight( int[,] piece )
        //        {
        //            int[,] res = new int[piece.GetLength(1),piece.GetLength(0)];
        //            for (int x = 0; x < piece.GetLength(1); x++)
        //            {
        //                for (int y = piece.GetLength(0)-1; y >= 0; y--)
        //                {
        //                    res[x,y] = piece[y,x];
        //                    res[x,piece.GetLength(1)-y] = piece[y,x];
        //                }
        //            }
        //            return res;
        //        }

        /// <summary>
        /// Toggles the display between full screen and not full screen.
        /// If the current resolution doesn't match any resolution in
        /// the set of valid resolutions this method will choose and
        /// set a new resolution, from the set of valid resolutions
        /// that has the largest width.
        /// </summary>
        public void SetFullScreen()
        {
            bool foundValidResolution = false;
            int largestWidth = 0;
            int largestHeight = 0;

            // Get current resolution.
            int currentWidth = Screen.width;
            int currentHeight = Screen.height;

            // Loop through the valid resolutions.
            foreach (Resolution res in Screen.resolutions)
            {
                // Save away the largest valid resolution in case we 
                // don't find a match with a valid resolution.
                // TODO: Maybe find a better algorithm for finding the
                //       largest valid resolution.
                if (res.width > largestWidth)
                {
                    largestWidth = res.width;
                    largestHeight = res.height;
                }

                if ((currentWidth == res.width) && (currentHeight == res.height))
                {
                    // We have found a match between current and valid resolution.
                    foundValidResolution = true;
                    currentWidth = res.width;
                    currentHeight = res.height;
                    break;
                }
            }

            if (false == foundValidResolution)
            {
                // We did not find a match between the current 
                // resolution and the valid resolutions.
                currentWidth = largestWidth;
                currentHeight = largestHeight;
            }

            // Toggle the full screen mode.
            //Screen.SetResolution(currentWidth, currentHeight, !Screen.fullScreen);
            Screen.SetResolution(Screen.width, Screen.height, !Screen.fullScreen);
        }
    }
}