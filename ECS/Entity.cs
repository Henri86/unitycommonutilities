﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public List<ComponentBase> Components { get; private set; } = new List<ComponentBase>();

    public T GetComponentData<T>() where T : ComponentBase
    {
        return Components.OfType<T>().FirstOrDefault();
    }

    public ComponentBase this[Type componentType]
    {
        get { return Components.FirstOrDefault(component => component.GetType() == componentType); }
    }

    private void Awake()
    {
        Components = FindComponents().ToList();
        World.Instance.AddEntity(this);
    }

    private IEnumerable<ComponentBase> FindComponents()
    {
        return GetComponents<ComponentBase>();
    }

    private void OnDestroy()
    {
        World.Instance.RemoveEntity(this);
    }


    public IEnumerable<Type> GetComponenTypes()
    {
        return Components.Select(x => x.GetType());
    }

    public void ComponentAdded(ComponentBase component)
    {
        Components.Add(component);
        World.Instance.UpdateAffectedSystems(this);
    }

    public void ComponentRemoved(ComponentBase componentToRemove)
    {
        Components.RemoveAll(component => component.GetInstanceID() == componentToRemove.GetInstanceID());
        World.Instance.UpdateAffectedSystems(this);
    }

    public bool HasAnyComponent(IEnumerable<Type> types)
    {
        var hasAnyComponent = Components.Select(x => x.GetType()).Intersect(types).Any();
        return hasAnyComponent;
    }
    
    public bool HasAllComponent(IEnumerable<Type> types)
    {
        var components = Components.Select(x => x.GetType());
        var count = components.Intersect(types).Count();
        var hasAllComponent = count == types.Count();
        return hasAllComponent;
    }
}