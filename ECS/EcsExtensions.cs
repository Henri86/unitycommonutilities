﻿using UnityEngine;

namespace Assets.UnityCommonUtilities.ECS
{
    public static class EcsExtensions
    {
        public static Entity GetEntity(this GameObject gameObject) => gameObject.GetComponent<Entity>();
    }
}
