﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.UnityCommonUtilities;
using UnityEngine;

public class World : MonoBehaviour
{
    private readonly List<SystemBase> systems = new List<SystemBase>();
    private readonly List<Entity> entities = new List<Entity>();

    public T GetSystem<T>() where T : SystemBase
    {
        return systems.OfType<T>().FirstOrDefault();
    }
    
    public IEnumerable<SystemBase> GetSystems(IEnumerable<Type> types)
    {
        return systems.Where(system => system.RelevanTypes.Intersect(types).Any());
    }

    public void AddSystem(SystemBase system)
    {
        systems.Add(system);
        system.Entities = GetEntities(system.RelevanTypes);
    }

    public void RemoveSystem(SystemBase system)
    {
        systems.Remove(system);
        system.Entities = GetEntities(system.RelevanTypes);
    }

    public void AddEntity(Entity entity)
    {
        entities.Add(entity);
        UpdateAffectedSystems(entity);
    }

    public void UpdateAffectedSystems(Entity entity)
    {
        var affectedSystems = GetSystems(entity.GetComponenTypes());
        foreach (var system in affectedSystems)
        {
            system.Entities = GetEntities(system.RelevanTypes);
        }
    }

    public void RemoveEntity(Entity entity)
    {
        entities.Remove(entity);
        UpdateAffectedSystems(entity);
    }

    public IEnumerable<Entity> GetEntities(IEnumerable<Type> types)
    {
        var entitiesToReturn = new List<Entity>();
        foreach (var entity in entities)
        {
            if (entity.HasAllComponent(types))
            {
                entitiesToReturn.Add(entity);
            }
        }
        return entitiesToReturn;
    }

    #region Singleton
    private static World _instance;
    public static World Instance
    {
        get
        {
            if (_instance != null) return _instance;
            _instance = FindObjectOfType<World>();
            if (_instance != null) return _instance;
            var obj = new GameObject {hideFlags = HideFlags.HideAndDontSave};
            _instance = obj.AddComponent<World>();
            return _instance;
        }
    }
    #endregion    
}