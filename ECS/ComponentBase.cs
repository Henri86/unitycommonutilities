﻿using UnityEngine;

[RequireComponent(typeof(Entity))]
public abstract class ComponentBase : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Entity>().ComponentAdded(this);
    }

    private void OnDestroy()
    {
        GetComponent<Entity>().ComponentRemoved(this);
    }
}