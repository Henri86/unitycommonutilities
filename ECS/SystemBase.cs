﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class SystemBase : MonoBehaviour
{
    public abstract Type[] RelevanTypes { get; }

    public IEnumerable<Entity> Entities { get; set; }

    protected virtual void Awake()
    {
        World.Instance.AddSystem(this);
    }

    protected virtual void Update()
    {
        if (Entities != null)
        {
            UpdateEntities(Entities);
        }
    }

    protected abstract void UpdateEntities(IEnumerable<Entity> entities);

    private void OnDestroy()
    {
        World.Instance.RemoveSystem(this);
    }
}