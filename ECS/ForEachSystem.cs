﻿using System.Collections.Generic;

public abstract class ForEachSystem : SystemBase
{
    public virtual void StartOfFrame()
    {

    }

    protected override void UpdateEntities(IEnumerable<Entity> entities)
    {
        StartOfFrame();
        foreach(var entity in entities)
        {
            UpdateEntity(entity);
        }
        //entities.ForEach(UpdateEntity);
        EndOfFrame();
    }

    protected virtual void UpdateEntity(Entity entity)
    {
        
    }

    public virtual void EndOfFrame()
    {

    }
}