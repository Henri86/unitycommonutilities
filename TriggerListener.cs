﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TriggerListener : MonoBehaviour
{
    public event TriggerEnterHandler TriggerEnter;
    public delegate void TriggerEnterHandler(Collider other);

    public event TriggerExitHandler TriggerExit;
    public delegate void TriggerExitHandler(Collider other);

    public event TriggerStayHandler TriggerStay;
    public delegate void TriggerStayHandler(Collider other);

    private void OnTriggerEnter(Collider other) {
        if (TriggerEnter != null) { TriggerEnter(other); }
    }

    private void OnTriggerExit(Collider other) {
        if (TriggerExit != null) { TriggerExit(other); }
    }

    private void OnTriggerStay(Collider other) {
        if (TriggerStay != null) { TriggerStay(other); }
    }
}