﻿using UnityEngine;

public static class VectorIntExtensions
{
    public static Vector3 ToVector3(this Vector2Int vector) => new Vector3(vector.x, vector.y);
    public static Vector3Int ToVector3Int(this Vector2Int vector2Int) => new Vector3Int(vector2Int.x, vector2Int.y, 0);
    public static Vector2Int ToVector2Int(this Vector3Int vector) => new Vector2Int((int) vector.x, (int) vector.y);
}