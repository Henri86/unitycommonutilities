﻿using System;
using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    public static class VectorExtensions
    {
        public static Vector3 Random(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(UnityEngine.Random.Range(min,max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max));
        }

        public static Vector3 RandomX(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(UnityEngine.Random.Range(min, max), aVecotor.y, aVecotor.z);
        }

        public static Vector3 RandomY(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(aVecotor.x, UnityEngine.Random.Range(min, max), aVecotor.z);
        }

        public static Vector3 RandomZ(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(aVecotor.x, aVecotor.y, UnityEngine.Random.Range(min, max));
        }

        public static Vector3 RandomXy(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), aVecotor.z);
        }

        public static Vector3 RandomXz(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(UnityEngine.Random.Range(min, max), aVecotor.y, UnityEngine.Random.Range(min, max));
        }

        public static Vector3 RandomYz(this Vector3 aVecotor, float min, float max)
        {
            return new Vector3(aVecotor.x, UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max));
        }

        public static Vector3 Round(this Vector3 aVecotor)
        {
            return aVecotor.Round(0);
        }

        public static Vector3 Round(this Vector3 aVecotor, int aDecimals)
        {
            return new Vector3((float) Math.Round(aVecotor.x, aDecimals), (float) Math.Round(aVecotor.y, aDecimals), (float) Math.Round(aVecotor.z, aDecimals));
        }

        public static void ForEachXY(this Vector2Int vector2Int, Action<int, int> callback)
        {
            for (int y = 0; y < vector2Int.y; y++)
            for (int x = 0; x < vector2Int.x; x++)
                callback(x, y);
        }
    }
}
