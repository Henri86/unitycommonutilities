﻿using Unity.Collections;

public static class NativeArrayExtensions
{
    public static T GetElement<T>(this NativeArray<T> array, int x, int y, int width) where T : struct
    {
        return array[width * x + y];
    }

    public static void SetElement<T>(this NativeArray<T> array, int x, int y, int width, T value) where T : struct
    {
        array[width * x + y] = value;
    }
}