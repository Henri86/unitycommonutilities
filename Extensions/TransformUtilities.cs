﻿using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.UnityCommonUtilities
{
    public static class TransformUtilities
    {
        public static void SmoothLookAt(this Transform aTransform, Transform target, float speed)
        {
            aTransform.SmoothLookAt(target.position, speed);
        }

        public static void SmoothLookAt(this Transform aTransform, Vector3 targetPosition, float speed)
        {
            var newDirection = targetPosition - aTransform.position;
            if (newDirection == Vector3.zero)
                return;
            aTransform.rotation = Quaternion.Lerp(aTransform.rotation, Quaternion.LookRotation(newDirection), speed * Time.deltaTime);
        }

        public static Transform Clear(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                if (Application.isEditor)
                    Object.DestroyImmediate(child.gameObject);
                else
                    Object.Destroy(child.gameObject);
            }
            return transform;
        }

        public static void ResetTransformation(this Transform trans)
        {
            trans.position = Vector3.zero;
            trans.localRotation = Quaternion.identity;
            trans.localScale = new Vector3(1, 1, 1);
        }

        public static Bounds FindRenderBounds(this Transform aTransform)
        {
            var bounds = new Bounds();
            var first = true;
            foreach (var rend in aTransform.GetComponentsInChildren<Renderer>())
            {
                if (first)
                {
                    bounds = rend.bounds;
                    first = false;
                }
                else
                {
                    bounds.Encapsulate(rend.bounds);
                }
            }
            return bounds;
        }

        public static Transform[] GetChildren(this Transform transform)
        {
            var results = new List<Transform>();
            foreach (Transform child in transform)
            {
                results.Add(child);
            }
            return results.ToArray();
        }

        public static GameObject[] GetChildrenGameObjects(this Transform transform)
        {
            var results = new List<GameObject>();
            foreach (GameObject child in transform)
            {
                results.Add(child);
            }
            return results.ToArray();
        }
    }
}
