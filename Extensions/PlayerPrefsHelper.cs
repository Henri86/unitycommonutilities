﻿using UnityEngine;

namespace Assets.UnityCommonUtilities.Extensions
{
    public static class PlayerPrefsHelper
    {
        public static void SetBool(string aName, bool aValue)
        {
            PlayerPrefs.SetInt(aName, aValue ? 1 : 0);
        }

        public static bool GetBool(string aName) 
        {
            return PlayerPrefs.GetInt(aName)==1;
        }

        public static bool GetBool(string aName, bool aDefaultValue) 
        {
            if (PlayerPrefs.HasKey(aName)) 
            {
                return GetBool(aName);
            }
            return aDefaultValue;
        }
    }
}
