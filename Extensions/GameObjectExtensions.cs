﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.UnityCommonUtilities
{
    public static class GameObjectExtensions
    {
        public static Vector3 GetCenter(this GameObject[] gameObjects)
        {
            var totalX = 0f;
            var totalY = 0f;
            var totalZ = 0f;
            foreach (var player in gameObjects)
            {
                totalX += player.transform.position.x;
                totalY += player.transform.position.y;
                totalZ += player.transform.position.z;
            }
            var centerX = totalX / gameObjects.Length;
            var centerY = totalY / gameObjects.Length;
            var centerZ = totalZ / gameObjects.Length;
            return new Vector3(centerX, centerY, centerZ);
        }

        public static void SetLayerRecursively(this GameObject gameObject, int layer)
        {
            gameObject.layer = layer;
            foreach (Transform t in gameObject.transform)
                t.gameObject.SetLayerRecursively(layer);
        }

        public static void SetCollisionRecursively(this GameObject gameObject, bool tf)
        {
            var colliders = gameObject.GetComponentsInChildren<Collider>();
            foreach (var collider in colliders)
                collider.enabled = tf;
        }

        public static T[] GetChildComponentsWithTag<T>(this GameObject gameObject, string tag)
        where T : Component
        {
            var results = new List<T>();

            if (gameObject.CompareTag(tag))
                results.Add(gameObject.GetComponent<T>());

            foreach (Transform t in gameObject.transform)
                results.AddRange(t.gameObject.GetChildComponentsWithTag<T>(tag));

            return results.ToArray();
        }

        public static GameObject[] GetChildGameObjectsWithTag(this GameObject gameObject, string tag)
        {
            var results = new List<GameObject>();

            if (gameObject.CompareTag(tag))
                results.Add(gameObject);

            foreach (Transform t in gameObject.transform)
                results.AddRange(t.gameObject.GetChildGameObjectsWithTag(tag));

            return results.ToArray();
        }

        public static GameObject GetChildGameObjectWithTag(this GameObject gameObject, string tag)
        {
            return GetChildGameObjectsWithTag(gameObject, tag).FirstOrDefault();
        }

        public static bool HasComponent<T>(this GameObject obj) where T : Component
        {
            return obj.GetComponent<T>() != null;
        }

        public static bool HasComponent(this GameObject obj, Type componentType)
        {
            return obj.GetComponent(componentType) != null;
        }

        public static void SetVisualRecursively(this GameObject gameObject, bool tf)
        {
            var renderers = gameObject.GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
                renderer.enabled = tf;
        }

        public static int GetCollisionMask(this GameObject gameObject, int layer = -1)
        {
            if (layer == -1)
                layer = gameObject.layer;

            var mask = 0;
            for (var i = 0; i < 32; i++)
                mask |= (Physics.GetIgnoreLayerCollision(layer, i) ? 0 : 1) << i;

            return mask;
        }

        public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
        {
            return go.AddComponent<T>().GetCopyOf(toAdd);
        }

        public static T GetSafeComponent<T>(this GameObject obj) where T : MonoBehaviour
        {
            var component = obj.GetComponent<T>();

            if (component == null)
            {
                Debug.LogError("Expected to find component of type "
                   + typeof(T) + " but found none", obj);
            }
            return component;
        }

        public static void RemoveComponent<TComponent>(this GameObject obj, bool immediate = false)
        {
            var component = obj.GetComponent<TComponent>();

            if (component != null)
            {
                if (immediate)
                {
                    Object.DestroyImmediate(component as Object, true);
                }
                else
                {
                    Object.Destroy(component as Object);
                }
            }
        }

        public static void SortChildren(this GameObject o, Comparison<Transform> compare)
        {
            var children = o.GetComponentsInChildren<Transform>(true).ToList();
            children.Remove(o.transform);
            children.Sort(compare);
            for (int i = 0; i < children.Count; i++)
                children[i].SetSiblingIndex(i);
        }
    }
}
