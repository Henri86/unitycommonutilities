﻿using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    public static class ColorExtensions
    {
        public static float Dist(this Color color, Color colorToDiff)
        {
            var redDifference = color.r - colorToDiff.r;
            var greenDifference = color.g - colorToDiff.g;
            var blueDifference = color.b - colorToDiff.b;

            return (redDifference * redDifference + greenDifference * greenDifference + blueDifference * blueDifference) / 3.0f;
        }
    }
}
