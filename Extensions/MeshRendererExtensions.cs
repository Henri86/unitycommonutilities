﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class MeshRendererExtensions
{
    public static void RemoveMaterial(this MeshRenderer renderer, Predicate<Material> match)
    {
        var newMaterials = new List<Material>(renderer.materials);
        newMaterials.RemoveAll(match);
        renderer.materials = newMaterials.ToArray();
    }
}