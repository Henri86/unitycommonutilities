﻿using System;

public static class CommonExtensions
{
    public static float[,] Add(this float[,] data, float[,] dataToAdd)
    {
        data.ForEachXy((x, y) => data[x, y] += dataToAdd[x, y]);
        return data;
    }

    public static float[,] Add(this float[,] data, float dataToAdd)
    {
        data.ForEachXy((x, y) => data[x, y] += dataToAdd);
        return data;
    }

    public static float[,] Mul(this float[,] data, float[,] dataToAdd)
    {
        data.ForEachXy((x, y) => data[x, y] *= dataToAdd[x, y]);
        return data;
    }

    public static float[,] Mul(this float[,] data, float enhanceValue)
    {
        data.ForEachXy((x, y) => data[x, y] *= enhanceValue);
        return data;
    }

    public static float[,] Invert(this float[,] data)
    {
        data.Mul(-1f);
        return data;
    }

    public static void ForEachXy<T>(this T[,] data, Action<int, int> callback)
    {
        for (var y = 0; y < data.GetLength(1); y++)
        for (var x = 0; x < data.GetLength(0); x++)
            callback(x, y);
    }

    public static T[,] SetAll<T>(this T[,] data, T value)
    {
        data.ForEachXy((x, y) => data[x,y] = value);
        return data;
    }

    public static float[,] Lerp(this float[,] first, float[,] second, float t, Func<float, string, bool> predicate = null)
    {
        if (first.GetLength(0) != second.GetLength(0) || first.GetLength(1) != second.GetLength(1))
        {
            //Something is wrong
        }

//        var face = predicate(10, "");

        return null;
    }
}
