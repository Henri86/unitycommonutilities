﻿using UnityEngine;

public static class RigidBodyExtensions
{
    public static float GetSpeed(this Rigidbody rigidbody)
    {
        return rigidbody.velocity.magnitude;
    }

    public static Vector3 GetLocalVelocity(this Rigidbody rigidbody)
    {
        return rigidbody.transform.InverseTransformDirection(rigidbody.velocity);
    }

    public static float GetForwardSpeed(this Rigidbody rigidbody)
    {
        return rigidbody.GetLocalVelocity().z;
    }
}
