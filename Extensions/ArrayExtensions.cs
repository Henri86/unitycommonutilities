﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ArrayExtensions
{
    public static void ForEach<T>(this T[] items, Action<T> action)
    {
        foreach (var item in items)
        {
            action.Invoke(item);
        }
    }

    public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
    {
        foreach (var item in items)
        {
            action.Invoke(item);
        }
    }

    public static int[][] Reverse2DimArray(this int[][] theArray)
    {
        for (var rowIndex = 0; rowIndex <= (theArray.GetUpperBound(0)); rowIndex++)
        {
            for (var colIndex = 0; colIndex <= (theArray[0].GetUpperBound(0) / 2); colIndex++)
            {
                var tempHolder = theArray[rowIndex][colIndex];
                theArray[rowIndex][colIndex] = theArray[rowIndex][theArray[0].GetUpperBound(0) - colIndex];
                theArray[rowIndex][theArray[0].GetUpperBound(0) - colIndex] = tempHolder;
            }
        }
        return theArray;
    }

    public static float Median(this IEnumerable<float> source)
    {
        // Create a copy of the input, and sort the copy
        var temp = source.ToArray();
        Array.Sort(temp);

        var count = temp.Length;
        if (count == 0)
        {
            throw new InvalidOperationException("Empty collection");
        }
        if (count % 2 != 0) return temp[count / 2];
        // count is even, average two middle elements
        var a = temp[count / 2 - 1];
        var b = temp[count / 2];
        return (a + b) / 2f;
        // count is odd, return the middle element
    }
}

public static class ListExtensions
{
    public static Queue<T> AsQueue<T>(this IEnumerable<T> objects)
    {
        var toReturn = new Queue<T>();

        if (objects == null) return toReturn;

        foreach (var eventData in objects)
            toReturn.Enqueue(eventData);

        return toReturn;
    }
}