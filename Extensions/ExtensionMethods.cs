﻿
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public static class ExtensionMethods {

        public static T GetComponentInFirstRowChild<T>(this Transform transform)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                T component = transform.GetChild(i).GetComponent<T>();
                if (component != null)
                    return component;
            }
            return default(T);
        }

    }
}
