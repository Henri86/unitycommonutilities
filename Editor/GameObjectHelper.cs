﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class GameObjectHelper
{
    public static void ForEachSelected<T>(Action<T> action, bool includeChildrenz = false) where T : Component
    {
        var selectedGameObjects = Selection.gameObjects;
        var selectedRigidbodies = selectedGameObjects.Select(o => includeChildrenz ? o.GetComponentsInChildren<T>() : o.GetComponents<T>());
        selectedRigidbodies.ForEach(obj => obj.ForEach(action.Invoke));
    }
}
