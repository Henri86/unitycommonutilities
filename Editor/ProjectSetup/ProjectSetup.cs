﻿using System.IO;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProjectSetup : EditorWindow
{
    private static string[] packages = new string[]
    {
        "com.unity.textmeshpro",
        "com.unity.probuilder"
    };

    static AddRequest Request;

    [MenuItem("Tools/Project Setup")]
    public static void ShowWindow()
    {
        EditorWindow projectSetupWindow = GetWindow(typeof(ProjectSetup));
        projectSetupWindow.name = "Face";
    }

    public string[] setupAssetFolders = { "_Scenes", "Animations", "Audio", "Fonts", "Prefabs", "Resources", "Scripts", "Sprites", "Textures", "Materials", "Models" };

    void OnGUI()
    {
        #region FOLDERS
        GUILayout.Label("Folders", EditorStyles.boldLabel);

        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        SerializedProperty stringsProperty = so.FindProperty("setupAssetFolders");
        EditorGUILayout.PropertyField(stringsProperty, true);
        so.ApplyModifiedProperties();

        string gamePath = "Assets/_Game/";

        if (GUILayout.Button("Create folders"))
        {
            if (!Directory.Exists("Build"))
            {
                Directory.CreateDirectory("Build");
            }

            foreach (string folder in setupAssetFolders)
            {
                if (!Directory.Exists(gamePath + "/" + folder))
                {
                    Directory.CreateDirectory($"{gamePath}/{folder}");
                }
            }
        }
        #endregion
        
        #region GIT
        GUILayout.Label("GIT", EditorStyles.boldLabel);

        if (GUILayout.Button("Create .gitignore"))
        {
            CreategitIgnore(true);
        }

        if (GUILayout.Button("Show .gitignore"))
        {
            string ignoreFileName = ".gitignore";
            string to = Application.dataPath.Replace("/Assets", "");
            to += "/" + ignoreFileName;
            if (File.Exists(to))
            {
                Application.OpenURL(to);
            }
            else
            {
                if (EditorUtility.DisplayDialog(".gitignore does not exist.", "Do you want to create a new .gitignore ", "Yes", "No"))
                {
                    CreategitIgnore(true);
                }
            }
        }
        #endregion

        #region Scene
        GUILayout.Label("Scene", EditorStyles.boldLabel);
        if (GUILayout.Button("Create Main scene"))
        {
            Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
            EditorSceneManager.SaveScene(newScene, gamePath + "_Scenes/Main.unity");
        }
        #endregion

        #region CommonPackages
        GUILayout.Label("Common Packages", EditorStyles.boldLabel);
        foreach (var packageName in packages)
        {
            if (GUILayout.Button(packageName))
            {
                Request = Client.Add(packageName);
                EditorApplication.update += Progress;
            }
        }
        #endregion
    }

    static void Progress()
    {
        if (Request.IsCompleted)
        {
            if (Request.Status == StatusCode.Success)
                Debug.Log("Installed: " + Request.Result.packageId);
            else if (Request.Status >= StatusCode.Failure)
                Debug.Log(Request.Error.message);

            EditorApplication.update -= Progress;
        }
    }

    private void CreategitIgnore(bool aShowFile = false)
    {
        string ignoreFileName = ".gitignore";
        string from = Application.dataPath + "/CommonUtilities/Editor/ProjectSetup" + "/" + ignoreFileName;
        string to = Application.dataPath.Replace("/Assets", "");
        to += "/" + ignoreFileName;
        if (!File.Exists(to))
        {
            File.Copy(from, to);
        }

        if (aShowFile)
        {   
            Application.OpenURL(to);
        }
    }
}