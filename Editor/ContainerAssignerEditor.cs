﻿using System;
using Assets.UnityCommonUtilities.System;
using UnityEditor;

namespace Assets.UnityCommonUtilities.Editor
{
    [CustomEditor(typeof(ContainerAssigner))]
    public class ContainerAssignerEditor : UnityEditor.Editor
    {
        int _choiceIndex;
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            ContainerAssigner containerAssigner = target as ContainerAssigner;

            // Set the choice index to the previously selected index
            if (containerAssigner != null)
            {
                string[] _choices = UnityEditorInternal.InternalEditorUtility.tags;
                _choiceIndex = Array.IndexOf(_choices, containerAssigner.myAsssignedContainerTag);

                // If the choice is not in the array then the _choiceIndex will be -1 so set back to 0
                if (_choiceIndex < 0)
                {
                    _choiceIndex = 0;
                }

                _choiceIndex = EditorGUILayout.Popup("Container tag", _choiceIndex, _choices);

                // Update the selected choice in the underlying object
                containerAssigner.myAsssignedContainerTag = _choices[_choiceIndex];
            }

            // Save the changes back to the object
            EditorUtility.SetDirty(target);
        }
    }
}