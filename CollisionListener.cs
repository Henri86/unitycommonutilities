﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CollisionListener : MonoBehaviour
{
    public event CollisionExitHandler CollisionExit;
    public event CollisionEnterHandler CollisionEnter;
    public event CollisionStayHandler CollisionStay;

    public delegate void CollisionExitHandler(Collision collision);
    public delegate void CollisionEnterHandler(Collision collision);
    public delegate void CollisionStayHandler(Collision collision);
    
    private void OnCollisionEnter(Collision collision) {
        CollisionEnter?.Invoke(collision);
    }

    private void OnCollisionExit(Collision collision) {
        CollisionExit?.Invoke(collision);
    }

    private void OnCollisionStay(Collision collision) {
        CollisionStay?.Invoke(collision);
    }
}