﻿using System.Linq;
using UnityEngine.UI;

namespace Assets.UnityCommonUtilities
{
    public static class ToggleGroupExtension
    {
        public static Toggle GetActive(this ToggleGroup aGroup)
        {
            return aGroup.ActiveToggles().FirstOrDefault();
        }

        public static Toggle[] GetToggles(this ToggleGroup aGroup)
        {
            return aGroup.gameObject.GetComponentsInChildren<Toggle>();
        }

        public static void ClearToggles(this ToggleGroup aGroup)
        {
            foreach (var toggle in aGroup.GetToggles())
                toggle.isOn = false;
        }
    }
}
