﻿using System;
using UnityEngine;

namespace Assets.UnityCommonUtilities
{
    [Serializable]
    public class ScaleBinding
    {
        public KeyCode Key;
        public float Scale;
    }

    public class TimeManager : UnitySingleton<TimeManager>
    {
        private const string TotalTimePlayed = "TotalTimePlayed";
        
        [SerializeField]
        private ScaleBinding[] scales;

        [SerializeField]
        private float startScale = 1.0f;

        private float timeSinceLastSave;
        private bool oneFrame;
        private ScaleBinding _currentScaleBinding;

        private void Start()
        {
            Time.timeScale = startScale;
            InvokeRepeating("UpdateTimePlayed", 0, 10);
        }

        private void Update()
        {
            if (oneFrame)
            {
                Time.timeScale = 0.0f;
                oneFrame = false;
            }

            foreach (var scaleBinding in scales)
            {
                if (Input.GetKeyDown(scaleBinding.Key))
                {
                    if (Time.timeScale == 0.0f && scaleBinding.Scale == 0.0f)
                    {
                        Time.timeScale = 1.0f;
                        oneFrame = true;
                    }
                    else
                    {
                        CurrentScaleBinding = scaleBinding;
                    }

                    break;
                }
            }
        }

        public ScaleBinding CurrentScaleBinding
        {
            get => _currentScaleBinding;
            set
            {
                _currentScaleBinding = value;
                Time.timeScale = _currentScaleBinding.Scale;
            }
        }

        public void StartGameTime()
        {
            Time.timeScale = startScale;
        }

        /// <summary>
        /// Use with caution. Performance expensive
        /// </summary>
        public float GetTimePlayed()
        {
            UpdateTimePlayed();
            return PlayerPrefs.GetFloat(TotalTimePlayed);
        }

        /// <summary>
        /// Use with caution. Performance expensive
        /// </summary>
        private void UpdateTimePlayed()
        {
            float totalTimePlayed = PlayerPrefs.GetFloat(TotalTimePlayed);
            totalTimePlayed += Time.realtimeSinceStartup - timeSinceLastSave;
            timeSinceLastSave = Time.realtimeSinceStartup;
            //            totalTimePlayed += Time.time;
            PlayerPrefs.SetFloat(TotalTimePlayed, totalTimePlayed);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Use with caution. Performance expensive
        /// </summary>
	    private void ResetTimePlayed()
        {
            PlayerPrefs.SetFloat(TotalTimePlayed, 0);
            PlayerPrefs.Save();
        }
    }
}
