﻿using UnityEngine;

namespace Assets.UnityCommonUtilities
{
	public abstract class UnitySingletonPersistent<T> : MonoBehaviour where T : Component
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        var obj = new GameObject();
                        obj.hideFlags = HideFlags.HideAndDontSave;
                        instance = obj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        public virtual void Awake()
        {
            var duplicates = FindObjectsOfType<T>();
            if (duplicates.Length > 1)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            if (instance == null)
            {
                instance = this as T;
                OnFirstAwake();
            }
            else
            {
                Destroy(gameObject);
            }
            gameObject.name += $"[Persistent]";
        }

	    protected virtual void OnFirstAwake()
        {

        }
    }
}