using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor (typeof(ErrorCatcher))]
public class ErrorCatcherEditor : Editor
{ 
	private ErrorCatcher errorCatcher;
 
	public override void OnInspectorGUI ()
	{ 
		errorCatcher = (ErrorCatcher)target;
        DrawDefaultInspector();

        if (GUILayout.Button("Open Player.log"))
        {
            ErrorCatcher.OpenPlayerLogFile();
        }
    }
}
