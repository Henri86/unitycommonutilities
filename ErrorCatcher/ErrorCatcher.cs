using System;
using System.Collections.Generic;
using UnityEngine;

public class ErrorCatcher : MonoBehaviour
{
    [SerializeField]
    private List<LogType> catchTypes = new List<LogType>()
    {
        LogType.Error, LogType.Exception
    };

    private int openedCount;

    private void OnEnable()
    {
        Application.logMessageReceived += LogCallback;
    }

    private void LogCallback(string condition, string stackTrace, LogType type)
    {
        if (!Application.isEditor && openedCount <= 0 && catchTypes.Contains(type))
        {
            OpenPlayerLogFile();
            openedCount++;
        }
    }

    public static void OpenPlayerLogFile()
    {
        var userProfilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        var path = $"file://{userProfilePath}//AppData//LocalLow//{Application.companyName}//{Application.productName}//Player.log";
        Application.OpenURL(path);
    }

    private void OnDisable()
    {
        Application.logMessageReceived -= LogCallback;
    }
}
