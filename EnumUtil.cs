﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

public static class EnumUtil
{
    public static T RandomEnumValue<T>()
    {
        var values = Enum.GetValues(typeof(T));
        int random = Random.Range(0, values.Length);
        return (T)values.GetValue(random);
    }

    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException($"Argumnent {typeof(T).FullName} is not an Enum");

        T[] arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf(arr, src) + 1;
        return (arr.Length == j) ? arr[0] : arr[j];
    }

    public static T Prev<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException($"Argumnent {typeof(T).FullName} is not an Enum");

        T[] arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf(arr, src) - 1;
        return (-1 == j) ? arr[arr.Length-1] : arr[j];
    }
}
