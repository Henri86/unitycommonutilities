﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.SwipeHandling
{
    public enum SwipeDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    public class SwipeScript : MonoBehaviour
    {
        [SerializeField]
        private List<ISwipeListener> swipeListeners;

        private float fingerStartTime;
        private Vector2 fingerStartPos = Vector2.zero;

        private bool isSwipe;
        private float minSwipeDist = 50.0f;
        private float maxSwipeTime = 0.5f;

        public void Update()
        {
            if (Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            /* this is a new touch */
                            isSwipe = true;
                            fingerStartTime = Time.time;
                            fingerStartPos = touch.position;
                            break;

                        case TouchPhase.Canceled:
                            /* The touch is being canceled */
                            isSwipe = false;
                            break;

                        case TouchPhase.Ended:

                            float gestureTime = Time.time - fingerStartTime;
                            float gestureDist = (touch.position - fingerStartPos).magnitude;

                            if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist)
                            {
                                Vector2 direction = touch.position - fingerStartPos;
                                Vector2 swipeType = Vector2.zero;

                                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                                {
                                    // the swipe is horizontal:
                                    swipeType = Vector2.right * Mathf.Sign(direction.x);
                                }
                                else
                                {
                                    // the swipe is vertical:
                                    swipeType = Vector2.up * Mathf.Sign(direction.y);
                                }

                                if (swipeType.x != 0.0f)
                                {
                                    if (swipeType.x > 0.0f)
                                    {
                                        // MOVE RIGHT
                                        foreach (ISwipeListener swipeListener in swipeListeners)
                                        {
                                            swipeListener.OnSwipe(SwipeDirection.Right);
                                        }
                                    }
                                    else
                                    {
                                        // MOVE LEFT
                                        foreach (ISwipeListener swipeListener in swipeListeners)
                                        {
                                            swipeListener.OnSwipe(SwipeDirection.Left);
                                        }
                                    }
                                }

                                if (swipeType.y != 0.0f)
                                {
                                    if (swipeType.y > 0.0f)
                                    {
                                        // MOVE UP
                                        foreach (ISwipeListener swipeListener in swipeListeners)
                                        {
                                            swipeListener.OnSwipe(SwipeDirection.Up);
                                        }
                                    }
                                    else
                                    {
                                        // MOVE DOWN
                                        foreach (ISwipeListener swipeListener in swipeListeners)
                                        {
                                            swipeListener.OnSwipe(SwipeDirection.Down);
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }
        
        public void AddListener(ISwipeListener aSwipeListener)
        {
            if (swipeListeners == null){
                swipeListeners = new List<ISwipeListener>();
            }
            swipeListeners.Add(aSwipeListener);
        }

        public void RemoveSwipeListner(ISwipeListener aSwipeListener)
        {
            if (swipeListeners == null){
                return;
            }
            foreach (ISwipeListener swipeListener in swipeListeners)
            {
                if (swipeListener == aSwipeListener)
                {
                    swipeListeners.Remove(aSwipeListener);
                }
            }
        }

        public void ClearListeners()
        {
            if (swipeListeners == null){
                return;
            }
            swipeListeners.Clear();
        }
    }
}
