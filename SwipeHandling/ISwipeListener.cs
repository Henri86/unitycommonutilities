﻿namespace Assets.Scripts.SwipeHandling
{
    public interface ISwipeListener
    {
        void OnSwipe(SwipeDirection aSwipeDirection);
    }
}
