﻿using System;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-99)]
public class Scheduler : MonoBehaviour
{
    private static object actionsLock = new object();

    private static Dictionary<object, object> taskIds = new Dictionary<object, object>();

    //private static List<Func<bool>> updateActions = new List<Func<bool>>();

    private static List<Func<bool>> updateActions = new List<Func<bool>>();

    private static Deque<Func<bool>> updateActionsToProcess = new Deque<Func<bool>>();

    private static List<DelayedAction> nextFrameActions = new List<DelayedAction>();

    private static Deque<DelayedAction> nextFrameActionsToProcess = new Deque<DelayedAction>();

    private static List<DelayedAction> delayedActions = new List<DelayedAction>();

    private static List<Action> delayedActionsToInvoke = new List<Action>();

    private static List<Action> lateUpdateActionsToInvoke = new List<Action>();

    private static bool initialized;

    private void Awake()
    {
        initialized = true;
    }

    void Update()
    {
        lock (actionsLock)
        {
            updateActionsToProcess.AddRange(updateActions);
        }
        while (!updateActionsToProcess.IsEmpty)
        {
            Func<bool> updateAction = updateActionsToProcess.RemoveFront();
            bool keep = updateAction.Invoke();
            if (!keep)
            {
                lock (actionsLock)
                {
                    updateActions.Remove(updateAction);
                }
            }
        }

        //for (int i = updateActions.Count - 1; i >= 0; i--)
        //{
        //    if (updateActions.Count <= i)
        //    {
        //        break;
        //    }
        //    Func<bool> updateAction = updateActions[i];
        //    if (!updateAction.Invoke())
        //    {
        //        lock (actionsLock)
        //        {
        //            if (i < updateActions.Count && updateActions[i] == updateAction)
        //            {
        //                updateActions.RemoveAt(i);
        //            }
        //        }
        //    }
        //}

        lock (actionsLock)
        {
            for (int i = 0; i < nextFrameActions.Count; i++)
            {
                DelayedAction nextFrameAction = nextFrameActions[i];
                nextFrameAction.executeAfterFrames--;
                if (nextFrameAction.executeAfterFrames <= 0)
                {
                    nextFrameActionsToProcess.Add(nextFrameAction);
                    nextFrameActions.RemoveAt(i);
                    i--;
                }
            }
        }
        while (!nextFrameActionsToProcess.IsEmpty)
        {
            nextFrameActionsToProcess.RemoveFront().action.Invoke();
        }

        bool hasDelayedActions = false;
        lock (actionsLock)
        {
            hasDelayedActions = delayedActions.Count != 0;
        }
        if (hasDelayedActions)
        {
            float time = Time.time;
            lock (actionsLock)
            {
                for (int i = 0; i < delayedActions.Count; i++)
                {
                    if (time >= delayedActions[i].executeTime)
                    {
                        delayedActionsToInvoke.Add(delayedActions[i].action);
                        delayedActions.RemoveAt(i);
                    }
                }
            }

            for (int i = 0; i < delayedActionsToInvoke.Count; i++)
            {
                delayedActionsToInvoke[i].Invoke();
            }
            delayedActionsToInvoke.Clear();
        }
    }

    private void LateUpdate()
    {
        for (int i = 0; i < lateUpdateActionsToInvoke.Count; i++)
        {
            Action action = lateUpdateActionsToInvoke[i];
            action.Invoke();
            lateUpdateActionsToInvoke.RemoveAt(i);
        }
    }

    public static void InvokeOnLateUpdate(Action action)
    {
        lateUpdateActionsToInvoke.Add(action);
    }

    public static DelayedAction InvokeNextFrame(Action action)
    {
        return InvokeAfterFrames(1, action);
    }

    public static DelayedAction InvokeAfterFrames(int count, Action action)
    {
        CheckInitialized();
        if (action == null)
        {
            throw new ArgumentNullException();
        }
        lock (actionsLock)
        {
            DelayedAction delayedAction = new DelayedAction
            {
                executeAfterFrames = count,
                action = action
            };
            nextFrameActions.Add(delayedAction);
            return delayedAction;
        }
    }

    public static DelayedAction InvokeAfter(float delay, Action action)
    {
        CheckInitialized();
        return InvokeAt(Time.time + delay, action);
    }

    public static DelayedAction InvokeDelayed(float delay, Action action)
    {
        return InvokeAfter(delay, action);
    }

    public static DelayedAction InvokeAt(float time, Action action)
    {
        CheckInitialized();
        if (action == null)
        {
            throw new ArgumentNullException();
        }
        lock (actionsLock)
        {
            DelayedAction delayedAction = new DelayedAction
            {
                executeTime = time,
                action = action
            };
            delayedActions.Add(delayedAction);
            return delayedAction;
        }
    }

    public static void InvokeNow(Action action)
    {
        CheckInitialized();
        action.Invoke();
    }

    public static bool Cancel(DelayedAction delayedAction)
    {
        CheckInitialized();
        lock (actionsLock)
        {
            bool removed = delayedActions.Remove(delayedAction);
            return removed;
        }
    }

    public static void CancelAll()
    {
        CheckInitialized();
        lock (actionsLock)
        {
            delayedActions.Clear();
        }
    }

    public static Func<bool> AddForDuration(float duration, Action<float> onUpdate, Action onFinish = null)
    {
        return AddForDuration(null, duration, null, onUpdate, onFinish);
    }

    public static Func<bool> AddForDuration(object id, float duration, Action<float> onUpdate, Action onFinish = null)
    {
        return AddForDuration(id, duration, null, onUpdate, onFinish);
    }

    public static Func<bool> AddForDuration(float duration, Action onStart, Action<float> onUpdate, Action onFinish)
    {
        return AddForDuration(null, duration, onStart, onUpdate, onFinish);
    }

    public static Func<bool> AddForDuration(object id, float duration, Action onStart, Action<float> onUpdate, Action onFinish)
    {
        float startTime = Time.time;
        bool started = false;
        bool func()
        {
            float time = Time.time;
            if (time >= startTime + duration)
            {
                onUpdate.Invoke(1.0f);
                onFinish?.Invoke();
                return false;
            }
            else
            {
                if (!started)
                {
                    onStart?.Invoke();
                }
                onUpdate.Invoke((time - startTime) / duration);
                return true;
            }
        }
        return AddOnUpdate(id, func);
    }

    public static Func<bool> AddForDuration(float duration, Action<float> onUpdate, Action onFinish, params (float progress, Action<float> onEvent)[] onEvents)
    {
        return AddForDuration(null, duration, null, onUpdate, onFinish, onEvents);
    }

    public static Func<bool> AddForDuration(object id, float duration, Action<float> onUpdate, Action onFinish, params (float progress, Action<float> onEvent)[] onEvents)
    {
        return AddForDuration(id, duration, null, onUpdate, onFinish, onEvents);
    }

    public static Func<bool> AddForDuration(float duration, Action onStart, Action<float> onUpdate, Action onFinish, params (float progress, Action<float> onEvent)[] onEvents)
    {
        return AddForDuration(null, duration, onStart, onUpdate, onFinish, onEvents);
    }

    public static Func<bool> AddForDuration(object id, float duration, Action onStart, Action<float> onUpdate, Action onFinish, params (float progress, Action<float> onEvent)[] onEvents)
    {
        float startTime = Time.time;
        bool started = false;
        int eventIndex = 0;
        bool func()
        {
            float time = Time.time;
            float progress = (time - startTime) / duration;
            if (eventIndex < onEvents.Length && progress >= onEvents[eventIndex].progress)
            {
                onEvents[eventIndex].onEvent?.Invoke(progress);
                eventIndex++;
            }
            if (time >= startTime + duration)
            {
                onUpdate.Invoke(1.0f);
                onFinish?.Invoke();
                return false;
            }
            else
            {
                if (!started)
                {
                    onStart?.Invoke();
                }
                onUpdate.Invoke(progress);
                return true;
            }
        }
        return AddOnUpdate(id, func);
    }

    public static Func<bool> AddOnUpdate(Func<bool> action)
    {
        return AddOnUpdate(null, action);
    }

    public static bool RemoveOnUpdate(Func<bool> action)
    {
        CheckInitialized();
        lock (actionsLock)
        {
            return updateActions.Remove(action);
        }
    }

    public static void RemoveAllOnUpdate()
    {
        CheckInitialized();
        lock (actionsLock)
        {
            updateActions.Clear();
        }
    }

    public static Func<bool> AddOnUpdate(object id, Func<bool> action)
    {
        CheckInitialized();
        lock (actionsLock)
        {
            updateActions.Add(action);
            if (id != null)
            {
                taskIds.Add(id, action);
            }
        }
        return action;
    }

    public static bool RemoveOnUpdate(object id)
    {
        CheckInitialized();
        lock (actionsLock)
        {
            if (taskIds.TryGetValue(id, out object task))
            {
                taskIds.Remove(id);
                updateActions.Remove((Func<bool>)task);
                return true;
            }
        }
        return false;
    }

    public static void Clear()
    {
        lock (actionsLock)
        {
            taskIds.Clear();
            updateActions.Clear();
            nextFrameActions.Clear();
            delayedActions.Clear();
        }
    }

    private static void CheckInitialized()
    {
        if (!initialized)
        {
            throw new System.Exception("Scheduler must be added to a GameObject!");
        }
    }

    private void OnDestroy()
    {
        Clear();
    }
}
