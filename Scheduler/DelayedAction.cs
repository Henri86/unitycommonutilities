﻿using System;

public class DelayedAction
{
    public float executeTime;

    public int executeAfterFrames;

    public Action action;

    public void Cancel()
    {
        Scheduler.Cancel(this);
    }
}
